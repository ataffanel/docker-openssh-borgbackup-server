# docker-openssh-borgbackup-server

Docker container with bord and openssh-server.
Based on linuxserver.io's openssh-server docker image.
The only addition is the installation of the alpine package borgbackup.

See the original [docker-openssh-server](https://github.com/linuxserver/docker-openssh-server) for more informations about ssh setup.
